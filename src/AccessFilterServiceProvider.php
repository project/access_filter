<?php

namespace Drupal\access_filter;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Alters container services.
 */
class AccessFilterServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    parent::alter($container);

    if (!defined('Symfony\Component\HttpKernel\HttpKernelInterface::MAIN_REQUEST')) {
      $container->getDefinition('access_filter.middleware')
        ->setClass(AccessFilterLegacyMiddleware::class);
    }
  }

}
